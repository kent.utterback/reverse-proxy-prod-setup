#!/bin/bash

if ! [ -x "$(command -v docker-compose)" ]; then
  echo 'Error: docker-compose is not installed.' >&2
  exit 1
fi

domains=(digitaleclecticism.com www.digitaleclecticism.com)
rsa_key_size=4096
data_path="/etc/letsencrypt" # Location of the certs
email="utterback.kent.a@gmail.com" # Adding a valid address is strongly recommended
staging=1 # Set to 1 if you're testing your setup to avoid hitting request limits

if [ -d "$data_path" ]; then
  read -p "Existing data found for $domains. Continue and replace existing certificate? (y/N) " decision
  if [ "$decision" != "Y" ] && [ "$decision" != "y" ]; then
    exit
  fi
  mkdir -p "$data_path"
fi

nginx_ssl_options="https://raw.githubusercontent.com/certbot/certbot/master/certbot-nginx/certbot_nginx/tls_configs/options-ssl-nginx.conf"
ssl_dh_params="https://raw.githubusercontent.com/certbot/certbot/master/certbot/ssl-dhparams.pem"

if [ ! -e "$data_path/conf/options-ssl-nginx.conf" ] || [ ! -e "$data_path/conf/ssl-dhparams.pem" ]; then
  echo "### Downloading recommended TLS parameters ..."
  mkdir -p "$data_path/conf"
  if curl --output /dev/null --silent --head --fail "$nginx_ssl_options"; then
    curl -s "$nginx_ssl_options" > "$data_path/conf/options-ssl-nginx.conf"
  else 
    echo "Certbot NGINX config NOT FOUND"
  fi
  if curl --output /dev/null --silent --head --fail "$ssl_dh_params"; then
    curl -s "$ssl_dh_params" > "$data_path/conf/ssl-dhparams.pem"
  else 
    echo "SSL DH Params NOT FOUND"
  fi
  echo
fi

echo "### Creating dummy certificate for $domains ..."
path="$data_path/dummy/$domains"
mkdir -p "$data_path/dummy/$domains"

openssl req -x509 -nodes -newkey rsa:1024 -days 1\
    -keyout "$path/privkey.pem" \
    -out "$path/fullchain.pem" \
    -subj '/CN=localhost'

echo "### Starting reverseproxy ..."
docker-compose up --force-recreate --build -d reverseproxy
echo

echo "### Requesting Let's Encrypt certificate for $domains ..."
#Join $domains to -d args
domain_args=""
for domain in "${domains[@]}"; do
  domain_args="$domain_args -d $domain"
done

# Select appropriate email arg
case "$email" in
  "") email_arg="--register-unsafely-without-email" ;;
  *) email_arg="--email $email" ;;
esac

# Enable staging mode if needed
if [ $staging != "0" ]; then staging_arg="--staging"; fi

docker-compose run --rm --entrypoint "\
  certbot certonly --webroot -w /var/www/certbot \
    $staging_arg \
    $email_arg \
    $domain_args \
    --rsa-key-size $rsa_key_size \
    --agree-tos \
    --force-renewal" certbot
echo