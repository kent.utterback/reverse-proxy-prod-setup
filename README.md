# Reverse Proxy Setup

A reverse proxy setup for a production server. This project uses Certbot to handle certs from Let's Encrypt and NGINX as the reverse proxy to the rest of the applications on a custom Docker network.

## Security

Certbot handles the renewal and procurement of certs from Let's Encrypt and is shared on the Docker network via a Docker volume. This allows every app instance, multiple were planned if needed to scale, access to the wildcard cert to be used across the domain behind the reverse proxy server. This also allowed NGINX to pass HTTPS traffic to the app and the app to pass HTTPS traffic to the proxy creating end to end encryption, even within a private virtual network.